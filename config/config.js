// The endpoints for NYPL's Simplified Card Creator API
module.exports = {
  base: process.env.CARD_CREATOR_BASE_URL,
  username: 'validate/username',
  address: 'validate/address',
};
