# dgx-validations-service

This is the repository of the New York Public Library's patron creator validation microservice. The micorservice offers the API endpoints to check if the applicant enters a valid user name or a valid address to the "Get a Library Card" form.

The Link to the repository from [here](https://bitbucket.org/NYPL/dgx-validations-service).

The form on NYPL's website will fire a POST request to the service after it has been submitted. The service will then take the information and fire another POST request to NYPL Simplified's Card Creator API. It finally will reply the results based on the responses from the Card Creator API.

The Card Creator's documentation can be found [here](https://github.com/NYPL-Simplified/card-creator).

## Version
v0.1.0

## Technologies

  - [AWS Lambda](https://aws.amazon.com/lambda/) - The service will serve as an AWS Lambda instance.
  - [aws-serverless-express](https://github.com/awslabs/aws-serverless-express) - The server is built with ExpressJS with the npm module specifically for AWS Lambda.
  - [Swagger](http://swagger.io/) - The framework for the API documentation and architecture.
  - [node-lambda](https://www.npmjs.com/package/node-lambda) - The npm module helps us deploy this Express application as an AWS Lambda instance.
  - [helmet](https://helmetjs.github.io/docs/) - The npm module to improve security.
  - [yamljs](https://www.npmjs.com/package/yamljs) - The npm module helps convert the YAML swagger documentation to JSON format and vice versa.


## Install and Run

Clone the repo. Open your terminal and in the folder you just downloaded, run 
```sh
$ npm install
```

And then we need to install swagger globally, please run

```sh
$ npm install swagger -g
```

### Configuration
To setup the app configuration, copy the `.env.example` file to `.env` and update the necessary configuration parameters.

You need credentials for making a successful API call to NYPL's Simplied Card Creator.

*Please contact NYPL's Simplied Card Creator team if you need the credentials.*

### Start the service
To execute the service locally, run 
```sh
$ npm start
```
The server will be executed on _localhost:3001_. As the help from swagger, you don't need to restart the server to see the changes you made to the code.

### Call the APIs

You need credentials for making a successful API call to NYPL's Simplied Card Creator. You should set this credentials in the `.env` file. For example,

```javascript
CARD_CREATOR_USERNAME=username
CARD_CREATOR_PASSWORD=password
```

*Please contact [NYPL's Simplied Card Creator team](https://github.com/NYPL-Simplified/card-creator) if you need the credentials.*

### API Routes
#### User Name Validation

With a valid credential, now you can make a POST request to _localhost:3001/api/v0.1/validations/username_ for user name validation.

The request data format should be in JSON with the key "username". For instance,

```javascript
{"username": "mikeolson"}
```

A successful JSON response example will be as below,

```javascript
{
  "data": {
    "status_code_from_card_creator": 200,
    "valid": true,
    "type": "available-username",
    "card_type": "standard",
    "message": "This username is available.",
    "detail": {}
  }
}
```

#### Address Validation

Make a POST request to _localhost:3001/api/v0.1/validations/address_ for address validation.

The request data format should be in JSON with the key "address". For instance,

```javascript
{ 
  "address" : {
    "line_1" : "1123 fake Street",
    "city" : "New York",
    "state" : "NY",
    "zip" : "05150" 
  },
  "is_work_or_school_address" : true
}
```

A successful JSON response example will be as below,

```javascript
{
  "data": {
    "status_code_from_card_creator": 200,
    "valid": true,
    "type": "valid-address",
    "card_type": "standard",
    "message": "This valid address will result in a standard library card.",
    "detail": {},
    "address": {
      "line_1": "1123 fake St",
      "line_2": "",
      "city": "New York",
      "county": "New York",
      "state": "NY",
      "zip": "05150-2600",
      "is_residential": false
    },
    "original_address": {
      "line_1": "1123 fake Street",
      "line_2": "",
      "city": "New York",
      "county": "",
      "state": "NY",
      "zip": "05150",
      "is_residential": null
    }
  }
}
```

#### JSON Documentation

Visit _localhost:3001/docs/patron-validator_ for the JSON version of the service swagger documentation.

### Visit and Edit the Swagger Documentation

Visit _http://localhost:3001/docs_ to see your API service's documentation if executing the service locally(Be sure you have swagger installed globally already).

To edit the documentation with interactive UI, run this command below in your terminal.

```sh
$ npm run swagger-edit
```

It will automatically open the web page for you to edit.

After finishing the update, we need the npm module [yamljs](https://www.npmjs.com/package/yamljs) to convert our YAML swagger documentation to JSON format or vice versa. The JSON file is for NYPL's API Gateway documentation. Run

```sh
$ npm install -g yamljs
```

After it, you can run

```sh
$ npm run build-swagger-doc
```

This script will generate _swaggerDoc.json_ based on the YAML documentation.

## Deployment

To deploy the service as an AWS Lambda instance, we need the npm module [node-lambda](https://www.npmjs.com/package/node-lambda). Please go to the URL and install it globally by running

```sh
$ npm install -g node-lambda
```

Second, create the appropriate deploy .env file. For example to deploy to the QA environment, create the file deploy_qa.env and copy the deploy_env.env.example in the root of the folder to deploy_qa.env that you just created.

After setting up the "deploy_qa.env" and "deploy_production.env" files, run
```sh
$ npm deploy-package-qa
```
or
```sh
$ npm deploy-package-production
```

You can also run
```sh
$ npm run build-doc-deploy-package-qa
```
or
```sh
$ npm build-doc-deploy-package-production
```

*To get your AWS Lambda service credentials, please visit [AWS Lambda's website](https://aws.amazon.com/lambda/).*

It will convert the swagger YAML documenaton to JSON documentaion before deployment thus to make sure the instance has the latest documentation.

*To get your AWS Lambda service credentials, please visit AWS Lambda's website.*

## Development Change Log

### v0.1.0
#### Update
  - update the version tag for public beta release.

### v0.0.2
#### Add
  - change configuration to use environment.

### v0.0.1
#### Update
  - update the swagger documentation.
  - update the data structures of the responses.
  - update environment variables detection to disable port listening on Lambda.
#### Add
  - add the route to "/swagger" for JSON documentation.
  - add the middleware to support CORS.
